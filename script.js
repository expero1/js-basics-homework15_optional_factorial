/*
Реалізувати функцію підрахунку факторіалу числа. Завдання має бути виконане на чистому Javascript 
без використання бібліотек типу jQuery або React.

Технічні вимоги:

- Отримати за допомогою модального вікна браузера число, яке введе користувач.
- За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
- Використовувати синтаксис ES6 для роботи зі змінними та функціями.


Необов'язкове завдання підвищеної складності

Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число, 
або при введенні вказав не число, - запитати число заново 
(при цьому значенням для нього повинна бути введена раніше інформація).
*/
"use strict";
const isNumber = (string) => {
  return (typeof string === "string" && string.trim().length) ||
    typeof string === "number"
    ? !isNaN(Number(string))
    : false;
};

const getNumberWithPrompt = () => {
  let number = "";
  do {
    number = prompt("Enter number to calculate factorial", number);
    if (number === null) return null;
  } while (!isNumber(number));
  return parseInt(number);
};

const factorial = (n) => {
  if (n === 1 || n === 0) return 1;
  return n * factorial(n - 1);
};
// alternate syntax
//const factorial = (n) => (n === 1 || n === 0 ? 1 : n * factorial(n - 1));

const factorialCalculator = () => {
  const number = getNumberWithPrompt();
  if (number === null) return null;
  console.log(factorial(number));
};
factorialCalculator();

//console.log(factorial(100));
//console.log(factorial(1000));
//console.log(factorial2(100));
//console.log(factorial2(1000));
